function installSentry(url){
    if (!slon.isDebug){
        //отсылка ошибок в sentry
        Raven
            .config(url)
            .addPlugin(Raven.Plugins.Angular)
            .install();

        window.onerror = function (msg, url, lineNo, columnNo, error) {
            Raven.captureException(error, {message: msg});
        };

        slonComponents.directive('img', function($state, $location){
            return {
                restrict : 'E',
                link :  function ($scope, element, attrs){
                    element[0].onerror = function(e){
                        Raven.captureMessage('Image load error on '+$state.current.name, {
                            extra: {
                                src: this.src,
                                url : $location.absUrl()
                            }
                        });
                    }
                }
            }
        });

        slonComponents.directive('style', function($state, $location){
            var config = {attributes: true, childList: false, characterData: false};
            var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    if (mutation.attributeName === 'style'){
                        var url = mutation.target.style['backgroundImage'].slice(5,-2);
                        var image = new Image();
                        image.onerror = function(){
                            Raven.captureMessage('Image load error on '+$state.current.name, {
                                extra: {
                                    src: this.src,
                                    url : $location.absUrl()
                                }
                            });
                        };
                        image.src = url;
                    }
                });
            });

            return {
                restrict : 'A',
                compile :  function (tElement, tAttrs, transclude){
                    if ((tAttrs.style && tAttrs.style.indexOf('url') !== -1) ||
                        (tAttrs['ngStyle'] && tAttrs['ngStyle'].indexOf('url') !== -1)){
                        return function($scope, element, attrs){
                            observer.observe(element[0], config);
                        }
                    }
                }
            }
        });

        return true;
    }
    else{
        return false;
    }
}
